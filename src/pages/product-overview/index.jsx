import React, { Component } from 'react';

import Layout from 'components/layout';
import PageHero from 'components/page-hero';
import Grid from 'components/grid';
import ProductItem from 'components/product-item';

//images
//#region 
  import Bierverpakking from 'images/MVL-Bierverpakking-(1).png';
  import BrievenbusdoosA5 from 'images/Brievenbusdoos-A5-2.png';
  import BrievenbusdoosA4 from 'images/Brievenbusdoos-A4-1.png';
  import Flessendoos from 'images/MVL-Flessendoos-(1)_02.png';
  import Geschenkkoffer from 'images/MVL-Geschenkkoffer-(1).png';
  import Giftbox1 from 'images/Giftbox-1-2.png';
  import Giftbox2 from 'images/Giftbox-2-1.png';
  import Sulfaat from 'images/Sulfaat-Doos-2.png';
  import Wijndoos from 'images/Wijndoos-1.png';
  import Zalmdoos from 'images/Zalmdoos-1.png';
  import Bonbondoos from 'images/Bonbondoos-2.png';
  import Champagnedoos from 'images/Champagnedoos.png';
  import Header from 'images/Alle_producten_bij_elkaar_header.jpg';
//#endregion

//files
//#region
import WijndoosInfo from 'files/13010 - MVL Wijndoos - Informatie.pdf';
import WijndoosTemp from 'files/13010 - MVL Wijndoos.pdf';
import ChampagnedoosInfo from 'files/13011 - MVL Champagnedoos - Informatie.pdf';
import ChampagnedoosTemp from 'files/13011 - MVL Champagnedoos.pdf';
import Giftbox2Info from 'files/13020 - MVL Giftbox 2 - Informatie.pdf';
import Giftbox2Temp from 'files/13020 - MVL Giftbox 2.pdf';
import ZalmdoosInfo from 'files/13030 - MVL Zalmdoos - Informatie.pdf';
import ZalmdoosTemp from 'files/13030 - MVL Zalmdoos.pdf';
import FlessendoosInfo from 'files/13040 - MVL Flessendoos - Informatie.pdf';
import FlessendoosTemp from 'files/13040 - MVL Flessendoos.pdf';
import BonbondoosInfo from 'files/13050 - MVL Bonbondoos - Informatie.pdf';
import BonbondoosTemp from 'files/13050 - MVL Bonbondoos.pdf';
import GeschenkkofferInfo from 'files/13060 - MVL Geschenkkoffer - Informatie.pdf';
import GeschenkkofferTemp from 'files/13060 - MVL Geschenkkoffer.pdf';
import BierverpakkingInfo from 'files/13070 - MVL Bierverpakking - Informatie.pdf';
import BierverpakkingTemp from 'files/13070 - MVL Bierverpakking.pdf';
import Giftbox1Info from 'files/13080 - MVL Giftbox 1 - Informatie.pdf';
import Giftbox1Temp from 'files/13080 - MVL Giftbox 1.pdf';
import SulfaatInfo from 'files/13090 - MVL Sulfaat doos - Informatie.pdf';
import SulfaatTemp from 'files/13090 - MVL Sulfaat doos.pdf';
import BrievenbusdoosA4Info from 'files/13100 - MVL Brievenbusdoos A4 - Informatie.pdf';
import BrievenbusdoosA4Temp from 'files/13100 - MVL Brievenbusdoos A4.pdf';
import BrievenbusdoosA5Info from 'files/13101 - MVL Brievenbusdoos A5 - Informatie.pdf';
import BrievenbusdoosA5Temp from 'files/13101 - MVL Brievenbusdoos A5.pdf';
//#endregion

class ProductOverview extends Component {
  render() {
    return (
      <Layout background="#F0F0F5">
        <PageHero
          variants={['blue', 'product-overview']}
          title="Kerst in jouw jasje"
          description1="Wil jij een uniek cadeau voor jouw medewerkers of klanten? Dat kan! We hebben een aantal verpakkingen voor cadeaus ontwikkeld die perfect zijn voor de feestdagen. Een wijnverpakking, bierverpakking, luxe doos voor een cadeaubon... Uiteraard kun je de vormgeving helemaal zelf bepalen. Hoe verras jij jouw medewerkers of relaties met het perfecte kerstcadeau?"
          description2="Heb je interesse in één van de verpakkingen? Klik op het winkelwagentje om een offerte aan te vragen. We gaan met liefde voor je aan de slag. Binnen 8 dagen kun je jouw verpakking al in huis hebben (vraag hiervoor naar onze voorwaarden)!"
          description3="Staat de maat van jouw keuze er niet bij? Geen nood! We kunnen ook geheel op maat voor jou aan de slag gaan. We kunnen al je wensen waar maken, zolang ’ie maar van karton is ;-)"
          backgroundImage={Header}
        />
        <div>
          <div className="wrapper">
            <Grid className={['33', 'product-item-grid']}>
              <ProductItem
                image={Wijndoos}
                title="MVL Wijndoos"
                subtitle1="Product: 13010"
                subtitle2="Materiaal: 1,5 mm E-golf"
                subtitle3="Max. Inhoud (LxBxH mm): 76 x 76 x 320"
                price100="742"
                price250="876"
                price500="964"
                download1={WijndoosInfo}
                download2={WijndoosTemp}
                mailsubject="Wijndoos - product nr. 13010"
              />
              <ProductItem
                image={Champagnedoos}
                title="MVL Champagnedoos"
                subtitle1="Product: 13011"
                subtitle2="Materiaal: 1,5 mm E-golf"
                subtitle3="Max. Inhoud (LxBxH mm): 90 x 90 x 330"
                price100="794"
                price250="1002"
                price500="1277"
                download1={ChampagnedoosInfo}
                download2={ChampagnedoosTemp}
                mailsubject="Champagnedoos - product nr. 13011"
              />
              <ProductItem
                image={Giftbox2}
                title="MVL Giftbox 2"
                subtitle1="Product: 13020"
                subtitle2="Materiaal: 1,5 mm E-golf"
                subtitle3="Max. Inhoud (LxBxH mm): 106 x 106 x 146"
                price100="781"
                price250="912"
                price500="1135"
                download1={Giftbox2Info}
                download2={Giftbox2Temp}
                mailsubject="Giftbox 2 - product nr. 13020"
              />
              <ProductItem
                image={Zalmdoos}
                title="MVL Zalmdoos"
                subtitle1="Product: 13030"
                subtitle2="Materiaal: 1,5 mm E-golf"
                subtitle3="Max. Inhoud (LxBxH mm): 570 x 195 x 30"
                price100="812"
                price250="995"
                price500="1298"
                download1={ZalmdoosInfo}
                download2={ZalmdoosTemp}
                mailsubject="Zalmdoos - product nr. 13030"
              />
              <ProductItem
                image={Flessendoos}
                title="MVL Flessendoos"
                subtitle1="Product: 13040"
                subtitle2="Materiaal: 1,5 mm E-golf"
                subtitle3="Max. Inhoud (LxBxH mm): 263 x 86 x 86"
                price100="706"
                price250="869"
                price500="1139"
                download1={FlessendoosInfo}
                download2={FlessendoosTemp}
                mailsubject="Flessendoos - product nr. 13040"
              />
              <ProductItem
                image={Bonbondoos}
                title="MVL Bonbondoos"
                subtitle1="Product: 13050"
                subtitle2="Materiaal: 300g sulfaat"
                subtitle3="Max. Inhoud (LxBxH mm): 153 x 123 x 77"
                price100="739"
                price250="867"
                price500="1055"
                download1={BonbondoosInfo}
                download2={BonbondoosTemp}
                mailsubject="Bonbondoos - product nr. 13050"
              />
              <ProductItem
                image={Geschenkkoffer}
                title="MVL Geschenkkoffer"
                subtitle1="Product: 13060"
                subtitle2="Materiaal: 1,5 mm E-golf"
                subtitle3="Max. Inhoud (LxBxH mm): 153 x 80 x 246"
                price100="836"
                price250="1055"
                price500="1419"
                download1={GeschenkkofferInfo}
                download2={GeschenkkofferTemp}
                mailsubject="Geschenkkoffer - product nr. 13060"
              />
              <ProductItem
                image={Bierverpakking}
                title="MVL Bierverpakking"
                subtitle1="Product: 13070"
                subtitle2="Materiaal: 1,5 mm E-golf"
                subtitle3="Max. Inhoud (LxBxH mm): 136 x 64 x 243"
                price100="928"
                price250="1195"
                price500="1639"
                download1={BierverpakkingInfo}
                download2={BierverpakkingTemp}
                mailsubject="Bierverpakking - product nr. 13070"
              />
              <ProductItem
                image={Giftbox1}
                title="MVL Giftbox 1"
                subtitle1="Product: 13080"
                subtitle2="Materiaal: 1,5 mm E-golf"
                subtitle3="Max. Inhoud (LxBxH mm): 132 x 189 x 30"
                price100="781"
                price250="912"
                price500="1135"
                download1={Giftbox1Info}
                download2={Giftbox1Temp}
                mailsubject="Giftbox 1- product nr. 13080" 
              />
              <ProductItem
                image={Sulfaat}
                title="MVL Doos met losse deksel"
                subtitle1="Product: 13090"
                subtitle2="Materiaal: 300g sulfaat"
                subtitle3="Max. Inhoud (LxBxH mm): 175 x 105 x 41"
                price100="753"
                price250="767"
                price500="859"
                download1={SulfaatInfo}
                download2={SulfaatTemp}
                mailsubject="Doos met losse deksel - product nr. 13090"
              />
              <ProductItem
                image={BrievenbusdoosA4}
                title="MVL Brievenbusdoos A4"
                subtitle1="Product: 13100"
                subtitle2="Materiaal: 1,5 mm E-golf"
                subtitle3="Max. Inhoud (LxBxH mm): 319 x 252 x 28"
                price100="631"
                price250="756"
                price500="963"
                download1={BrievenbusdoosA4Info}
                download2={BrievenbusdoosA4Temp}
                mailsubject="Zalmdoos - product nr. 13100"
              />
              <ProductItem
                image={BrievenbusdoosA5}
                title="MVL Brievenbusdoos A5"
                subtitle1="Product: 13101"
                subtitle2="Materiaal: 1,5 mm E-golf"
                subtitle3="Max. Inhoud (LxBxH mm): 210 x 152 x 27"
                price100="582"
                price250="636"
                price500="737"
                download1={BrievenbusdoosA5Info}
                download2={BrievenbusdoosA5Temp}
                mailsubject="Brievenbusdoos A5 - product nr. 13101"
              />
            </Grid>
          </div>
        </div>
      </Layout>
    );
  }
}

export default ProductOverview;
