import React, { Component } from 'react';
import styles from './styles.scss';

class Prices extends Component {
  render() {
    const { price100, price250, price500} = this.props
    return (
      <div className={styles.pricecontainer}>
        <div className={styles.divider} />
        <span className={styles.title}>Prijzen</span>
        <span className={styles.subtitle}>
          Graag een andere oplage? Vraag gerust!
        </span>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <span>100 st.</span>
          <div>
            <span className={styles.price}>€{price100},-</span> <span className={styles.btw}>excl. btw</span>
          </div>
        </div>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <span>250 st.</span>
          <div>
            <span className={styles.price}>€{price250},-</span> <span className={styles.btw}>excl. btw</span>
          </div>
        </div>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
          <span>500 st.</span>
          <div>
            <span className={styles.price}>€{price500},-</span> <span className={styles.btw}>excl. btw</span>
          </div>
        </div>
      </div>
    );
  }
}

export default Prices;
