import React, { Component } from 'react';
import styles from './styles.scss';

class Button extends Component {
  classNames(classes) {
    let result = [];
    result.push('button');
    result.push(styles['button']);
    if (classes) {
      classes.forEach(function(variant) {
        result.push(styles[`button--${variant}`]);
      });
    }
    return result.join(' ');
  }

  render() {
    const {className, children } = this.props;

    return  (
      <span {...this.props} className={this.classNames(className)}>
        {children}
      </span>
    )
  }
}

export default Button;
