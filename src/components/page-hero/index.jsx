import React, { Component } from 'react';
import styles from './styles.scss';

import { Logo } from 'icons/logo';

class PageHero extends Component {
  classNames(variants) {
    let classes = [];
    classes.push(styles['page-hero']);
    if (variants) {
      variants.forEach(function(variant) {
        classes.push(styles[`page-hero--${variant}`]);
      });
    }
    return classes.join(' ');
  }

  render() {
    const {
      fronttitle,
      title,
      description1,
      description2,
      description3,
      backgroundImage,
      variants,
      children
    } = this.props;

    return (
      <div
        className={this.classNames(variants)}
        style={{
          backgroundImage: `url(
            ${backgroundImage}
          )`
        }}>
        <Logo className={styles.logo} />
        {variants &&
          variants.includes('simple') === false && (
            <div className={styles.wrapper}>
              <article className={styles['content']}>
                <h2
                  className={styles.fronttitle}
                  dangerouslySetInnerHTML={{ __html: fronttitle }}
                />
                <h1
                  className={styles.title}
                  dangerouslySetInnerHTML={{ __html: title }}
                />
                <p
                  className={styles.description}
                  dangerouslySetInnerHTML={{ __html: description1 }}
                />
                <p
                  className={styles.description}
                  dangerouslySetInnerHTML={{ __html: description2 }}
                />
                <p
                  className={styles.description}
                  dangerouslySetInnerHTML={{ __html: description3 }}
                />
                {children}
              </article>
            </div>
          )}
      </div>
    );
  }
}

export default PageHero;
