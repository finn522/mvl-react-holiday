import React, { Component } from 'react';
import styles from './styles.scss';
import { Helmet } from 'react-helmet';

class Layout extends Component {
  render() {
    const { background, children } = this.props;

    return (
      <div
        className={styles['default-layout']}
        style={{ background: background }}>
          <Helmet>
            <meta charSet="utf-8" />
            <title></title>
          </Helmet>
        {children}
      </div>
    );
  }
}

export default Layout;
