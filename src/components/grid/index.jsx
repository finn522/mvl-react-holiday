import React, { Component } from 'react';
import styles from './styles.scss';

class Grid extends Component {
  classNames(classes) {
    let result = [];
    result.push(styles['grid']);
    if (classes) {
      classes.forEach(function(variant) {
        result.push(styles[`grid--${variant}`]);
      });
    }
    return result.join(' ');
  }

  render() {
    const { children, className } = this.props;
    return <ul className={this.classNames(className)}>{children}</ul>;
  }
}

export default Grid;
