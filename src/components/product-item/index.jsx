import React, { Component } from 'react';
import styles from './styles.scss';

import { Download } from 'icons/download';
import { ArrowRight } from 'icons/arrow-right';
import { Cross } from 'icons/cross';
import { Cart } from 'icons/cart';
import Button from 'components/button';
import Prices from 'components/prices';

class ProductItem extends Component {

  resettooltip = event => {
    event.preventDefault();
    console.log(event.currentTarget.parentNode.getElementsByTagName('span')[1])
    if (event.currentTarget.parentNode.getElementsByTagName('span')[1].classList.contains(styles.hidetooltip) !== false){
      event.currentTarget.parentNode.getElementsByTagName('span')[1].classList.remove(styles.hidetooltip)
    }
  }

  closeTooltip = event => {
    event.preventDefault();
    event.currentTarget.parentNode.classList.add(styles.hidetooltip)
  }
  render() {
    const {
      image,
      title,
      subtitle1,
      subtitle2,
      subtitle3,
      price,
      mailsubject,
      price100,
      price250,
      price500,
      download1,
      download2,
    } = this.props;

    return (
      <li className={styles['product-item']}>
        <div className={styles.container}>
          <img className={styles.image} src={image} alt={title} />
          <div className={styles.heading}>
            <div className={styles.firstline}>
              <span className={styles.title}>{title}</span>
              <span className={styles.price}>
                <b>{price}</b>
              </span>
            </div>
            <div className={styles.secondline}>
              <div className={styles.subtitlecontainer}>
                <span className={styles.subtitle}>{subtitle1}</span>
                <span className={styles.subtitle}>{subtitle2}</span>
                <span className={styles.subtitle}>{subtitle3}</span>
              </div>
              <div className={styles.buttoncontainer}>
                <div className={styles.add1}>
                  <a onClick={this.resettooltip} href="#!">
                    <Button className={['green-outline']}>
                      <Download />
                    </Button>
                  </a>
                  <span className={`${styles.tooltip} ${styles.tooltiplarger}`}>
                    <span>
                      Welke bestand wil <br /> je downloaden?
                    </span>
                    <div className={styles.closebutton} onClick={this.closeTooltip}><Cross/></div>
                    <div className={styles.button1} >
                      <a href={download1} download >
                        <Button className={['large']}>
                          Download informatie <ArrowRight />
                        </Button>
                      </a>
                    </div>
                    <div className={styles.button2}>
                      <a href={download2} download>
                        <Button className={['large']}>
                          Download template <ArrowRight />
                        </Button>
                      </a>
                    </div>
                  </span>
                </div>
                <div className={styles.add2}>
                  <a href={'mailto:sales@mvl.nl?subject=' + mailsubject}>
                    <Button>
                      <Cart />
                    </Button>
                  </a>
                  <span className={styles.tooltip}>
                    <span>Bestel hier of vraag <br/> een offerte aan</span>
                  </span>
                </div>
              </div>
            </div>
            <Prices
              price100={price100}
              price250={price250}
              price500={price500}
            />
          </div>
        </div>
      </li>
    );
  }
}

export default ProductItem;
