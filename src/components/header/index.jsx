import React, { Component } from 'react';
import styles from './styles.scss';

import { Logo } from 'icons/logo';
class Header extends Component {
  render() {
    return (
      <header className={styles.header} id="header">
        <div className={styles.headerwrapper}>
            <Logo className={styles.logo} />
        </div>
      </header>
    );
  }
}

export default Header;
