import React from 'react';

import ProductOverview from 'pages/product-overview';


const App = () => (
  <div>
    <main>
       <ProductOverview/>
    </main>
  </div>
);

export default App;
