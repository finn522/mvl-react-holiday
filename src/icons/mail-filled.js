import React from 'react';
export const MailFilled = props => (
<svg width="24px" height="15px" viewBox="0 0 24 15" {...props}>
    <g id="Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g id="Nieuws_klantenservice" transform="translate(-1001.000000, -715.000000)" fill="#41A741">
            <path d="M1015.70733,723.465931 L1023.67898,730 L1002.3195,730 L1010.29419,723.465931 L1013,725.682848 L1015.70733,723.465931 Z M1016.88561,722.498523 L1025,715.850729 L1025,729.150748 L1016.88561,722.498523 Z M1001,715.850729 L1009.11591,722.498523 L1001,729.150748 L1001,715.850729 Z M1002.3195,715 L1023.67898,715 L1013,723.750985 L1002.3195,715 Z" id="Combined-Shape"></path>
        </g>
    </g>
</svg>
);