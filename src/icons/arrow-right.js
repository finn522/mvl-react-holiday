import React from 'react';
export const ArrowRight = props => (
    <svg viewBox="0 0 29 27" {...props}>
    <polygon
      fill="currentColor"
      fillRule="evenodd"
      points="15.16 0 11.77 3.31 19.81 11.16 0 11.16 0 15.84 19.81 15.84 11.77 23.69 15.16 27 29 13.5 15.16 0"
    />
  </svg>
);
