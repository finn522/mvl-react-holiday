import React from 'react';
export const Download = props => (
<svg width="17px" height="23px" viewBox="0 0 17 23" {...props}>
    <g id="Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g id="Procuct_pagina_standaard_producten" transform="translate(-399.000000, -1399.000000)" fill="#41AA41">
            <g id="Group-13" transform="translate(118.000000, 1385.000000)">
                <g id="Group-8">
                    <g id="Group-2" transform="translate(281.000000, 14.000000)">
                        <g id="Group-12">
                            <g id="Group">
                                <path d="M6.8690373,11.8143546 L6.8690373,0 L10.303556,0 L10.303556,11.6417614 L14.6179609,7.32735641 L16.9534677,9.66286317 L10.8355068,15.7808241 L10.8820391,15.8273564 L8.54653233,18.1628632 L8.5,18.1163308 L8.45346767,18.1628632 L6.1179609,15.8273564 L6.16449324,15.7808241 L0.0465323335,9.66286317 L2.3820391,7.32735641 L6.8690373,11.8143546 Z" id="Combined-Shape"></path>
                                <rect id="Rectangle-3" x="0" y="20" width="17" height="3"></rect>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </g>
</svg>
);

