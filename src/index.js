import React from 'react';
import { render } from 'react-dom';
import App from 'containers/app';

import 'sanitize.css/sanitize.css';
import 'styles/global.scss';
import 'fonts/fonts.css';
const target = document.querySelector('#root');

render(
      <App />,
      target
);
